package servlets;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import main.User;
import javax.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginUser extends HttpServlet
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");

            User user = User.authenticate(request.getParameter("username"), request.getParameter("password"));
            if (user == null) {
                PrintWriter out = response.getWriter();
                out.println("bad username or password!");
                out.println("<a href=login\"../\">Maybe try again?...</a>");//testwo
            }


            HttpSession session = request.getSession(true);
            session.setAttribute("username", user.getUsername());

            RequestDispatcher rd1 = request.getRequestDispatcher("start_login_page.jsp");
            rd1.forward(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
}

