
package servlets;

import main.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


@WebServlet("/userslist")
public class ShowUser extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        ArrayList<User> users = User.getUsers();

        PrintWriter out = response.getWriter();
        HttpSession usr = request.getSession(false);
        if (new User().getUser((String)usr.getAttribute("username")).isAdmin()){
            out.println("<form method='post'>");
            out.println("Username: <input type='text' name='username'/><br/>");
            out.println("<input type='submit' value='Toggle premium'/>");
            out.println("</form>");
        }
        out.println("<table>");
        out.println("<tr><th>Username</th><th>Email</th><th>Premium</th></tr>");
        for (User user : users) {
            out.print("<tr>");
            out.println("<td>" + user.getUsername() + "</td>");
            out.println("<td>" + user.getEmail() + "</td>");
            out.println("<td>" + user.isPremium() + "</td>");
            out.print("</tr>");
        }
        out.println("<form method='get' action='logout'><input type='submit' value='Logout'>");
        out.println("</table>");
        
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        User.togglePremium(request.getParameter("username"));
        response.sendRedirect(response.encodeRedirectURL("userslist"));
    }
}