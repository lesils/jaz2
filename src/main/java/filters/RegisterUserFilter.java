package filters;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

public class RegisterUserFilter implements Filter {

	@Override
	public void destroy() {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filter)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(true);
		String username = (String) session.getAttribute("username");
		
		if (username != null && !username.isEmpty()) {
			res.sendRedirect("/success.jsp");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {}

}
