
package filters;

import java.io.IOException;
import java.nio.file.DirectoryStream.Filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;

@WebFilter("/login")
public class LoginFilter implements Filter {

	
	public void destroy() {}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filter)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(true);
		String username = (String) session.getAttribute("username");
		
		if (username != null && !username.isEmpty()) {
			res.sendRedirect("/login");
		}
		filter.doFilter(request, response);
	}

	
	public void init(FilterConfig arg0) throws ServletException {}

	@Override
	public boolean accept(Object entry) throws IOException {
				return false;
	}



}

