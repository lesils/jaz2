<%@ page import="main.User" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Welcome Page</title>
</head>
<body>


<h1> My Profile </h1>

<%
    HttpSession sessionUser=request.getSession(false);
    String username=(String)sessionUser.getAttribute("username");

    User user = User.getUser(username);

    out.print("<h2>This is my profile, more information about me... </h2>");
    out.println("Username: " + user.getUsername() + "<br>");
    out.println("Email: " + user.getEmail() + "<br>");
    out.println("Is premium: " + user.isPremium() + "<br>");
%>

<br><br>
<form method="get" action="logout">
    <input type="submit" value="Logout">
</form>
<%
    if (user.isPremium())
    {
        out.println("<a href=\"userslist\"><button>premium permissions</button></a>");
    }
    %>

<br><br>


</body>
</html>
